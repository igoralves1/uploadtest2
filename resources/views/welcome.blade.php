<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="flex-center position-ref full-height">
                    <div class="title m-b-md">
                        <p>Laravel</p>
                        <p>
                            <div class="links">
                                <a href="{{ url('/upload') }}">Go to Upload Page</a>
                            </div>
                        </p>
                    </div>
                    <br>
                        <p>
                        <div class="links">
                            What was done?
                            Server: setup NGINX and php 7.1. Allow upload file until 1000M.
                            Laravel 5.7
                            Upload image, mp3 and video.
                            Security: CSRF, sql injection, base64_encode in teh databse,
                            DB: MariaDb db user can only insert and select in tables.
                            Setup of ubuntu 16.04 local and AWS-EC2. Free, 1G Ram (slow server)
                            AWS-EC2 just pull from bitbucket.
                            CLone - git clone https://igoralves1@bitbucket.org/igoralves1/uploadtest2.git
                        </div>
                        </p>
                </div>
            </div>
        </div>

        @foreach(array_chunk($imgs, 3) as $chunk)
        <div class="row">
            @foreach($chunk as $add)
            <div class="col-sm-4">
                @php
                $path=$add->path;
                $secret=$add->msg;

                $arrTemp = explode(".",$path);

                if($arrTemp[1]=='jpg' || $arrTemp[1]=='png' || $arrTemp[1]=='jpeg' || $arrTemp[1]=='gif'){
                echo "<h3>Image Title</h3>";
                echo "<p>Your message was stored in the DB as the follow secret:</p>";
                echo "<p>$secret</p>";
                echo "<img src='/storage/".$path."' class='img-fluid img-thumbnail g-rounded-3' alt='alt-Image' width='304' height='236'>";
                }
                elseif($arrTemp[1]=='mpga'){
                echo "<h3>Audio Title</h3>";
                echo "<p>Your message was stored in the DB as the follow secret:</p>";
                echo "<p>$secret</p>";
                echo "<audio controls='controls'><source src='/storage/".$path."' type='audio/mpeg'></audio>";
                }
                elseif($arrTemp[1]=='mp4'){
                echo "<h3>Video Title</h3>";
                echo "<p>Your message was stored in the DB as the follow secret:</p>";
                echo "<p>$secret</p>";
                echo "<video controls width='304' height='236' ><source src='/storage/".$path."'></video>";
                }
                elseif($arrTemp[1]=='pdf'){
                echo "<h3>Pdf Title</h3>";
                echo "<p>Your message was stored in the DB as the follow secret:</p>";
                echo "<p>$secret</p>";
                echo "<iframe src='/storage/".$path."' style='width:600px; height:500px;' frameborder='0'></iframe>";
                }
                @endphp
            </div>
            @endforeach
        </div>
        @endforeach

    </div>


    </body>
</html>
