<!DOCTYPE html>
<html lang="en">
<head>
    <title>Upload Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
    <h1>Upload Page</h1>
    <div class="links" id="myNavbar">
        <ul class="text-center">
            <li><a href="{{ URL::to('/') }}">Home</a></li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <form class="form-inline" action="{{ URL::to('/store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="fileToUpload">Example file input: mp3, mp4, png, jpeg, gif and PDF. Please note it is a demo server - too slow</label>
                    <br/>
                    <input id="fileToUpload" class="form-control-file" type="file" name="fileToUpload" >
                    <br/>
                </div>
                <br>
                <div class="form-group">
                    <label for="typesometid">Type Something:</label>
                    <input type="text" class="form-control" name="typesomething" id="typesometid">
                </div>
                <br>
                <div class="form-group">
                    <input id="btnSUbmit" class="btn btn-primary" type="submit" value="Submit" name="submit" disabled>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(
        function(){
            $('#fileToUpload').change(
                function(){
                    if ($(this).val()) {
                        $('#btnSUbmit').attr('disabled',false);
                    }
                }
            );
        });
</script>
</body>
</html>
