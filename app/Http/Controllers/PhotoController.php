<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgs = DB::table('tbl_upload')->get()->toArray();
//        dd($imgs);
        return view('welcome', compact('imgs'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = $request->file('fileToUpload')->store('public');
        $name=explode("/",$path);

        //Encoding message to the database. this is only a quick insight
        $secret='sdfsdh7sdhfksg7';//That comes from the user secret in teh DB
        $msgencoded = base64_encode($request->input('typesomething')."|$secret");
//        $msgdecoded = base64_decode($msgencoded,false);

        DB::insert('insert into tbl_upload (path, msg) values (?, ?)', ["$name[1]", "$msgencoded"]);

        return redirect('/');
    }
}
